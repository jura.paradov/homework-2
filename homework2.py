import sys, os
from hashlib import md5
from collections import defaultdict

def hash_file(filename):
    long_hash = md5()
    with open(filename, "rb") as b_file:
        cluster = b_file.read(4096)
        while cluster:
            long_hash.update(cluster)
            cluster = b_file.read(4096)
        return long_hash.hexdigest()

def make_dict(start_dir):
    defdict = defaultdict(list)
    for root, _ , files in os.walk(start_dir):
        for file in files:
            filename = os.path.join(root, file)
            if file[0] not in ['.', '~'] and not os.path.islink(filename):
                defdict[hash_file(filename)].append(filename)
    return defdict


def print_dict(to_print):
    for v in to_print.values():
        if len(v) > 1:
            print(':'.join(v))

start_dir = sys.argv[1]
result = make_dict(start_dir)
print_dict(result)
